#include <cuda.h>
#include <cuda_runtime.h>
#define DBG_ENABLE
#define GDBG_MALLOC
#define GDBG_MALLOC_PRINT
#include "gdebug.cuh"

__global__ void sample_kernel() {
    while (1) {
        GDBGL();
        if (threadIdx.x < 10) {
            GDBGTX("blabla", 0, 999, threadIdx.x);
            GDBGT("abcdef", 0, 999, threadIdx.x, 0, 0);
        }
        GDBGV("deadbeef :)", 101);
        GDBGFV("double", 0.123);
    }
}

void test_gdebug(void) {
    gdebug_init(0);

    cudaStream_t stream;
    CU_CHECK( cudaStreamCreate(&stream) );

    sample_kernel<<<1, 1024, 0, stream>>>();
    CU_CHECK( cudaGetLastError() );

    CU_CHECK( cudaDeviceSynchronize() );
    printf("test gdebug done\n");
}

void test_gdebug_malloc(void) {
    void *ptr[3];
    CU_CHECK( CUDA_MALLOC(&ptr[0], 100) );
    CU_CHECK( CUDA_MALLOC(&ptr[1], 123456) );
    CU_CHECK( CUDA_MALLOC(&ptr[2], 9999) );
    CU_CHECK( CUDA_FREE(ptr[0]) );
    CU_CHECK( CUDA_FREE(ptr[2]) );
    CU_CHECK( CUDA_FREE(ptr[1]) );
    printf("test gdebug_malloc done\n");
}

#define NTESTS 2
int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "usage: %s <test number: 0..%d>\n", argv[0], NTESTS - 1);
        exit(1);
    }
    switch (atoi(argv[1])) {
        case 0:
            test_gdebug();
            break;
        case 1:
            test_gdebug_malloc();
            break;
        default:
            fprintf(stderr, "unknown test\n");
            exit(1);
    }

    return 0;
}
