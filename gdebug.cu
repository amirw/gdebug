#include <cuda.h>
#include <cuda_runtime.h>
#include <pthread.h>
#include <stdio.h>
#include "gdebug.cuh"

#define DBG_MAX_STRING_LEN 40
#define DBG_MAX_FNAME_LEN 30
#define DBG_MAX_FUNC_LEN 40

#define MAX_DEVICES 100

struct gdebug_data_t {
    union {
        long int di;
        double df;
    };
    dbg_val_t t;
};

struct gdebug_t {
    int valid;
    long int line;
    long int id;
    struct gdebug_data_t data;
    int txyz[3];
    int bxyz[3];
    char s[DBG_MAX_STRING_LEN];
    char fname[DBG_MAX_FNAME_LEN];
    char func[DBG_MAX_FUNC_LEN];
};

           volatile struct gdebug_t *_hdbgarr[MAX_DEVICES];
__device__ volatile struct gdebug_t *_gdbg;
__device__ volatile int             _gdbg_mutex;

__host__ void *gdebug_loop(void *arg) {
    int dev_id = *(int *)arg;
    printf("debug loop awake. device %d\n", dev_id);
    CU_CHECK( cudaSetDevice(dev_id) );
    volatile struct gdebug_t *_hdbg = _hdbgarr[dev_id];
    while (1) {
        if (_hdbg->valid) {
            if (strlen((const char *)_hdbg->s) == 0) {
                strcpy((char *)_hdbg->s, "no string");
            }
            printf("D%d :: %s :: id = %ld :: ", dev_id, _hdbg->s, _hdbg->id);
            if (_hdbg->data.t == DBG_VAL_LONG) {
                printf("val = %ld :: hex = 0x%08lx :: ", _hdbg->data.di, _hdbg->data.di);
            } else if (_hdbg->data.t == DBG_VAL_DOUBLE) {
                printf("val = %lf :: ", _hdbg->data.df);
            }
            printf("Device = %d :: File = %s :: Func = %s :: Line = %ld :: B (%d %d %d) :: T (%d %d %d)\n",
                    dev_id,
                    _hdbg->fname, _hdbg->func, _hdbg->line,
                    _hdbg->bxyz[0], _hdbg->bxyz[1], _hdbg->bxyz[2],
                    _hdbg->txyz[0], _hdbg->txyz[1], _hdbg->txyz[2]);
            fflush(stdout);
            memset((void *)_hdbg->s, 0, DBG_MAX_STRING_LEN);
            memset((void *)_hdbg->fname, 0, DBG_MAX_FNAME_LEN);
            memset((void *)_hdbg->func, 0, DBG_MAX_FUNC_LEN);
            __sync_synchronize();
            _hdbg->valid = 0;
            __sync_synchronize();
        }
    }
}

__host__ void _gdebug_init(int device) {
    long int *gptr;
    int *dev_id = (int *) malloc(sizeof(int));
    *dev_id = device;
    int zero = 0;
    CU_CHECK( cudaSetDevice(device) );
    CU_CHECK( cudaHostAlloc(&_hdbgarr[device], sizeof(volatile struct gdebug_t), cudaHostAllocMapped) );
    memset((void *)_hdbgarr[device], 0, sizeof(struct gdebug_t));
    CU_CHECK( cudaHostGetDevicePointer(&gptr, (void *)_hdbgarr[device], 0) );
    CU_CHECK( cudaMemcpyToSymbol(_gdbg, &gptr, sizeof(volatile struct gdebug_t *), 0, cudaMemcpyHostToDevice) );
    CU_CHECK( cudaMemcpyToSymbol(_gdbg_mutex, &zero, sizeof(int), 0, cudaMemcpyHostToDevice) );
    __sync_synchronize();
    pthread_t thread;
    pthread_create(&thread, NULL, gdebug_loop, (void *)dev_id);
}

__device__ void __gdbg(const char *s,
                       long int id,
                       long int v,
                       double f,
                       dbg_val_t type,
                       int tx, int ty, int tz,
                       long int l,
                       const char *fname,
                       const char *func) {
    int mythread = (threadIdx.x == tx) && (threadIdx.y == ty) && (threadIdx.z == tz);
    if (mythread) {
        int retry = 1;
        while (retry) {
            int old;
            old = atomicExch((int *)&_gdbg_mutex, 1);
            if (old == 0) {
                if (!_gdbg->valid) {
                    _gdbg->line = l;
                    _gdbg->data.t = type;
                    if (type == DBG_VAL_LONG) {
                        _gdbg->data.di = v;
                    } else if (type == DBG_VAL_DOUBLE) {
                        _gdbg->data.df = f;
                    }
                    _gdbg->id = id;
                    _gdbg->txyz[0] = threadIdx.x;
                    _gdbg->txyz[1] = threadIdx.y;
                    _gdbg->txyz[2] = threadIdx.z;
                    _gdbg->bxyz[0] = blockIdx.x;
                    _gdbg->bxyz[1] = blockIdx.y;
                    _gdbg->bxyz[2] = blockIdx.z;
                    for (int i = 0; i < DBG_MAX_STRING_LEN; i++) {
                        if (s[i] == '\0')
                            break;
                        _gdbg->s[i] = s[i];
                    }
                    for (int i = 0; i < DBG_MAX_FNAME_LEN; i++) {
                        if (fname[i] == '\0')
                            break;
                        _gdbg->fname[i] = fname[i];
                    }
                    for (int i = 0; i < DBG_MAX_FUNC_LEN; i++) {
                        if (func[i] == '\0')
                            break;
                        _gdbg->func[i] = func[i];
                    }
                    __threadfence_system();
                    _gdbg->valid = 1;
                    __threadfence_system();
                    retry = 0;
                }
                atomicExch((int *)&_gdbg_mutex,0);
            }
        }
    }
}

