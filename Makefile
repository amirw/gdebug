CUDA_DIR=/usr/local/cuda
LIBCUDA=-L$(CUDA_DIR)/lib64 -lcudart -lcudadevrt
CUDAINC=-I$(CUDA_DIR)/include -I$(CUDA_DIR)/samples/common/inc/
NVCC=$(CUDA_DIR)/bin/nvcc -Xptxas="-v" --generate-code code=sm_35,arch=compute_35 -maxrregcount=32 -rdc=true
LIBS = -lpthread

all: sample

sample: sample.o gdebug.o gdebug_malloc.o
	$(NVCC) -o $@ $^ $(LIBS) $(LIBCUDA)

sample.o: sample.cu gdebug.cuh
	$(NVCC) $(CUDAINC) -c -o $@ $<

gdebug.o: gdebug.cu gdebug.cuh
	$(NVCC) $(CUDAINC) -c -o $@ $<

gdebug_malloc.o: gdebug_malloc.cu gdebug.cuh
	$(NVCC) $(CUDAINC) -c -o $@ $<

clean:
	\rm -f sample *.o *~
	\rm -rf *.dSYM
