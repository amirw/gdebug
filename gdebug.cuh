#ifndef __GDEBUG_CUH__
#define __GDEBUG_CUH__

#include <stdio.h>

#ifndef CU_CHECK
#define CU_CHECK(f) do {                        \
    cudaError_t e = f;                          \
    if (e != cudaSuccess) {                     \
        printf("Cuda failure %s:%d: '%s'\n", __FILE__, __LINE__, cudaGetErrorString(e));  \
        exit(0);                                \
    }                                           \
} while (0)
#endif

enum dbg_val_t {DBG_VAL_LONG = 0, DBG_VAL_DOUBLE = 1};
#if defined(DBG_ENABLE) && defined(__CUDA_ARCH__)
__device__ void __gdbg(const char *s,
                       long int id,
                       long int v,
                       double f,
                       dbg_val_t type,
                       int tx, int ty, int tz,
                       long int l,
                       const char *fname,
                       const char *func);
#define GDBGT(s, id, v, tx, ty, tz) __gdbg(s, id, v, 0, DBG_VAL_LONG, tx, ty, tz, __LINE__, __FILE__, __func__)
#define GDBGTX(s, id, v, tx) GDBGT(s, id, v, tx, 0, 0)
#define GDBGFT(s, id, f, tx, ty, tz) __gdbg(s, id, 0, f, DBG_VAL_DOUBLE, tx, ty, tz, __LINE__, __FILE__, __func__)
#define GDBGFTX(s, id, f, tx) GDBGFT(s, id, f, tx, 0, 0)
#else
#define GDBGT(s, id, v, tx, ty, tz)
#define GDBGTX(s, id, v, tx)
#define GDBGFT(s, id, f, tx, ty, tz)
#define GDBGFTX(s, id, f, tx)
#endif

__host__ void _gdebug_init(int device);

#define GDBG(s, id, v) GDBGT(s, id, v, 0, 0, 0)
#define GDBGL() GDBG("", 0, 0)
#define GDBGV(s, v) GDBG(s, 0, v)
#define GDBGF(s, id, f) GDBGFT(s, id, f, 0, 0, 0)
#define GDBGFV(s, f) GDBGF(s, 0, f)

#ifdef DBG_ENABLE
#define gdebug_init _gdebug_init
#else
#define gdebug_init(d)
#endif

#ifdef DBG_ENABLE
#define GDBG_COND(s, c) do {\
    if (!(c)) {\
        GDBGV("condition failed: ("s")"#c, 0);\
    }\
} while (0)
#else
#define GDBG_COND(s, c)
#endif

#ifdef DBG_ENABLE
#define GSTOP() do {} while (1)
#else
#define GSTOP()
#endif

#ifdef GDBG_MALLOC
#define CUDA_MALLOC(p, s) gdbg_malloc((void **)(p), s, __FILE__, __LINE__)
#define CUDA_FREE(p) gdbg_free((void **)(p), __FILE__, __LINE__)
cudaError_t gdbg_malloc(void **ptr, unsigned long size, const char f[], int l);
cudaError_t gdbg_free(void *ptr, const char f[], int l);
#else
#define CUDA_MALLOC cudaMalloc
#define CUDA_FREE cudaFree
#endif

#ifdef MALLOC
#undef MALLOC
#endif

#define MALLOC(s) ({\
        void *p = malloc(s);\
        if (!p)\
            fprintf(stderr, "%s :: %d :: malloc of %lu bytes faild\n", __FILE__, __LINE__, s);\
        p;\
})

#endif
