#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include "gdebug.cuh"
#include <pthread.h>

#define GDBG_MALLOC_MAX_ITEMS 100000

struct gdbg_malloc_item_t {
    void *ptr;
    unsigned long size;
    int valid;
};

struct gdbg_malloc_item_t *gdbg_malloc_items = NULL;
unsigned long gdbg_malloc_total = 0;
pthread_mutex_t gdbg_malloc_mutex = PTHREAD_MUTEX_INITIALIZER;

cudaError_t gdbg_malloc(void **ptr, unsigned long size, const char f[], int l) {
    cudaError_t ret;
    pthread_mutex_lock(&gdbg_malloc_mutex);
    if (!gdbg_malloc_items) {
        gdbg_malloc_items = (struct gdbg_malloc_item_t *)
            malloc(sizeof(gdbg_malloc_item_t) * GDBG_MALLOC_MAX_ITEMS);
        memset(gdbg_malloc_items, 0, sizeof(gdbg_malloc_item_t) * GDBG_MALLOC_MAX_ITEMS);
    }
    printf("cudaMalloc: f %-10s  l % 4d  already allocated %10lu  this size %10lu\n",
            f, l, gdbg_malloc_total, size);
    ret = cudaMalloc(ptr, size);
    gdbg_malloc_total += size;
    int i;
    for (i = 0; i < GDBG_MALLOC_MAX_ITEMS; i++) {
        if (!gdbg_malloc_items[i].valid) {
            gdbg_malloc_items[i].ptr = *ptr;
            gdbg_malloc_items[i].size = size;
            gdbg_malloc_items[i].valid = 1;
            break;
        }
    }
    if (i == GDBG_MALLOC_MAX_ITEMS) {
        fprintf(stderr, "GDBG MALLOC capacity full. change GDBG_MALLOC_MAX_ITEMS\n");
    }
    pthread_mutex_unlock(&gdbg_malloc_mutex);
    return ret;
}

cudaError_t gdbg_free(void *ptr, const char f[], int l) {
    cudaError_t ret;
    unsigned long size;
    pthread_mutex_lock(&gdbg_malloc_mutex);
    ret = cudaFree(ptr);
    int i;
    for (i = 0; i < GDBG_MALLOC_MAX_ITEMS; i++) {
        if (gdbg_malloc_items[i].valid && (gdbg_malloc_items[i].ptr == ptr)) {
            size = gdbg_malloc_items[i].size;
            gdbg_malloc_items[i].valid = 0;
            break;
        }
    }
    if (i == GDBG_MALLOC_MAX_ITEMS) {
        fprintf(stderr, "GDBG FREE couldn't find ptr %p\n", ptr);
    }
    printf("  cudaFree: f %-10s  l % 4d  already allocated %10lu  this size %10lu\n",
            f, l, gdbg_malloc_total, size);
    gdbg_malloc_total -= size;
    pthread_mutex_unlock(&gdbg_malloc_mutex);
    return ret;
}
